###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007-2011  IPFire Team  <info@ipfire.org>                     #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

VER        = 0.0.0-0db9d7f

THISAPP    = libsolv-$(VER)
DL_FILE    = $(THISAPP).tar.gz
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)
TARGET     = $(DIR_INFO)/$(THISAPP)

PROG       = libsolv
DEPS       = ""
PAK_VER    = 1

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE)

$(DL_FILE) = $(DL_FROM)/$(DL_FILE)

$(DL_FILE)_MD5 = ba95c404d807bc71297c508a7127a3d9

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

md5 : $(subst %,%_MD5,$(objects))

dist : 
	@$(PAK)

###############################################################################
# Downloading, checking, md5sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_MD5,$(objects)) :
	@$(MD5)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar zxf $(DIR_DL)/$(DL_FILE)

	# Who releases code with -Werror?
	cd $(DIR_APP) && sed -e "s/-Werror//g" -i CMakeLists.txt

	# Disable bindings, examples and tools.
	cd $(DIR_APP) && sed "/bindings/d" -i CMakeLists.txt
	cd $(DIR_APP) && sed "/examples/d" -i CMakeLists.txt
	cd $(DIR_APP) && sed "/tools/d" -i CMakeLists.txt

	# Make sure that libsatsolverext is linked properly to all needed
	# libraries.
	cd $(DIR_APP) && echo "TARGET_LINK_LIBRARIES(libsolvext libsolv \$${EXPAT_LIBRARY} \$${ZLIB_LIBRARY})" \
		>> ext/CMakeLists.txt

	# Remove the RPM stuff when we build with -DFEDORA=1 because we
	# do not support RPM.
	cd $(DIR_APP) && sed -e "s/DEBIAN/FEDORA/g" -i ext/CMakeLists.txt

	# Our version of glibc has no __qsort_r
	cd $(DIR_APP) && echo "#define USE_OWN_QSORT" >> src/util.h

	cd $(DIR_APP) && mkdir build
	cd $(DIR_APP)/build && cmake .. -DFEDORA=1 \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_SKIP_RPATH=1
	cd $(DIR_APP)/build && make $(PARALELLISMFLAGS)

	cd $(DIR_APP)/build && make install

	@rm -rf $(DIR_APP)
	@$(POSTBUILD)
