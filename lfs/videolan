###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

VER        = 0.8.6i
THISAPP    = vlc-$(VER)
DL_FILE    = $(THISAPP).tar.bz2
DL_FILE1   = $(THISAPP)-bugfix-080903.patch.bz2

DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)
TARGET     = $(DIR_INFO)/$(THISAPP)
PROG       = videolan
PAK_VER    = 5

DEPS       = "libshout libmpeg2 libmad libdvbpsi directfb ffmpeg faad2"

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE) $(DL_FILE1)

$(DL_FILE) = $(DL_FROM)/$(DL_FILE)
$(DL_FILE1) = $(DL_FROM)/$(DL_FILE1)

$(DL_FILE)_MD5 = 3c90520c9f22a68d287458d5a8af989e
$(DL_FILE1)_MD5 = 36ea293b5ebb604778b321b5e3e3cdc4

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

md5 : $(subst %,%_MD5,$(objects))

dist: 
	@$(PAK)

###############################################################################
# Downloading, checking, md5sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_MD5,$(objects)) :
	@$(MD5)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar jxf $(DIR_DL)/$(DL_FILE)
	#Apply some patched from videolan 8.6-bugfix branch,
	#ignore the error because one patch is already applied
	-cd $(DIR_APP) && bzip2 -d -c $(DIR_DL)/$(DL_FILE1) | patch -p1
	cd $(DIR_APP) && ./configure --prefix=/usr --enable-shared --with-ffmpeg-tree=/usr/src/ffmpeg \
								   --with-ffmpeg-config=/usr/src/ffmpeg \
								--disable-wxwidgets --disable-skins2 \
								--enable-httpd --enable-pth --enable-shout \
								--enable-smb --enable-cddax --enable-ncurses \
								--enable-a52 --enable-dts --enable-alsa \
								--enable-unicode-utf8 --enable-v4l --enable-vcd \
								--enable-ogg --enable-vorbis --enable-theora \
								--enable-mad --enable-v4l --enable-dvb \
								--with-dvb=/usr/src/v4l-dvb \
								--with-v4l=/usr/src/v4l-dvb \
								--enable-libdvbpsi --enable-faac --enable-mkv \
								--enable-x264 --enable-release --enable-sout \
								--disable-nls --enable-faad \
								--enable-directfb --with-directfb=/usr/src/directfb
	cd $(DIR_APP) && make $(MAKETUNING)
	cd $(DIR_APP) && make install
	@rm -rf $(DIR_APP)
	@$(POSTBUILD)
