###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

VER        = 2.6.12.0

THISAPP    = linux-libc-headers-$(VER)
DL_FILE    = $(THISAPP).tar.bz2
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)

ifeq "$(ROOT)" ""
ifeq "$(LFS_PASS)" "install"
  TARGET = $(DIR_INFO)/$(THISAPP)-install
else
  TARGET = $(DIR_INFO)/$(THISAPP)
endif
else
  TARGET = $(DIR_INFO)/$(THISAPP)-tools1
endif

###############################################################################
# Top-level Rules
###############################################################################
objects = $(DL_FILE)

$(DL_FILE)                             = $(DL_FROM)/$(DL_FILE)

$(DL_FILE)_MD5                         = eae2f562afe224ad50f65a6acfb4252c

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

md5 : $(subst %,%_MD5,$(objects))

###############################################################################
# Downloading, checking, md5sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_MD5,$(objects)) :
	@$(MD5)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar jxf $(DIR_DL)/$(DL_FILE)
ifeq "$(ROOT)" ""
ifeq "$(LFS_PASS)" "install"
	-mkdir -p /opt/$(MACHINE)-uClibc
	cd $(DIR_APP) && patch -Np1 < $(DIR_SRC)/src/patches/$(THISAPP)-inotify-3.patch
	cd $(DIR_APP) && install -dv /opt/$(MACHINE)-uClibc/usr/include/asm
	cd $(DIR_APP) && cp -Rv include/asm-i386/* /opt/$(MACHINE)-uClibc/usr/include/asm
	cd $(DIR_APP) && cp -Rv include/linux /opt/$(MACHINE)-uClibc/usr/include
	cd $(DIR_APP) && chown -Rv root:root /opt/$(MACHINE)-uClibc/usr/include/{asm,linux}
	cd $(DIR_APP) && find /opt/$(MACHINE)-uClibc/usr/include/{asm,linux} -type d -exec chmod -v 755 {} \;
	cd $(DIR_APP) && find /opt/$(MACHINE)-uClibc/usr/include/{asm,linux} -type f -exec chmod -v 644 {} \;
else
	cd $(DIR_APP) && patch -Np1 < $(DIR_SRC)/src/patches/$(THISAPP)-inotify-3.patch
	cd $(DIR_APP) && install -dv /usr/include/asm
	cd $(DIR_APP) && cp -Rv include/asm-i386/* /usr/include/asm
	cd $(DIR_APP) && cp -Rv include/linux /usr/include
	cd $(DIR_APP) && chown -Rv root:root /usr/include/{asm,linux}
	cd $(DIR_APP) && find /usr/include/{asm,linux} -type d -exec chmod -v 755 {} \;
	cd $(DIR_APP) && find /usr/include/{asm,linux} -type f -exec chmod -v 644 {} \;
endif
else
	cd $(DIR_APP) && cp -Rv include/asm-i386 /tools/include/asm
	cd $(DIR_APP) && cp -Rv include/linux /tools/include
endif
	@rm -rf $(DIR_APP)
	@$(POSTBUILD)
