###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007-2011  IPFire Team <info@ipfire.org>                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

VERSUFIX=ipfire$(KCFG)

VER        = 3.0-2

THISAPP    = compat-wireless-$(VER)
DL_FILE    = compat-wireless-$(VER).tar.bz2
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)
TARGET     = $(DIR_INFO)/$(THISAPP)-kmod-$(KVER)-$(VERSUFIX)

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE) asix-3.5.0a.tar.bz2

$(DL_FILE) = $(DL_FROM)/$(DL_FILE)
asix-3.5.0a.tar.bz2 = $(DL_FROM)/asix-3.5.0a.tar.bz2

$(DL_FILE)_MD5 = 7f2d0de30bf9c3e9a941d9e67971e9b3
asix-3.5.0a.tar.bz2_MD5=cc57513505d3ad1a43b58add91d2bba7

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

md5 : $(subst %,%_MD5,$(objects))

###############################################################################
# Downloading, checking, md5sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_MD5,$(objects)) :
	@$(MD5)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar xjf $(DIR_DL)/$(DL_FILE)

	# kfifo has no license info and taints kernel
	cd $(DIR_APP) && patch -Np1 < $(DIR_SRC)/src/patches/compat-wireless-2.6.39_kfifo_module_info.patch

	# Copy USB-Net drivers from Kernel...
	mkdir $(DIR_APP)/drivers/net/usb/new
	cp $(DIR_APP)/drivers/net/usb/*.c $(DIR_APP)/drivers/net/usb/new
	cp /usr/src/linux/drivers/net/usb/{*.c,*.h,Makefile} $(DIR_APP)/drivers/net/usb
	cp $(DIR_APP)/drivers/net/usb/new/*.c $(DIR_APP)/drivers/net/usb

	cd $(DIR_APP) && patch -Np1 < $(DIR_SRC)/src/patches/compat-wireless-2.6.34-usbnet_compile.patch

	# Build Updated asix usb-lan driver
	cd $(DIR_APP)/drivers/net/usb && tar xvf $(DIR_DL)/asix-3.5.0a.tar.bz2

ifeq "$(MACHINE_TYPE)" "arm"
	# B43+B44 driver doesn't build on arm
#	cd $(DIR_APP) && sed -i -e "s|CONFIG_B43=m||g" config.mk
#	cd $(DIR_APP) && sed -i -e "s|CONFIG_B44=m||g" config.mk
	# Add libartas uAP driver
	cd $(DIR_APP) && patch -Np1 < $(DIR_SRC)/src/patches/compat-wireless-3.0-libertas_uap.patch
	cd $(DIR_APP)&& echo CONFIG_LIBERTAS_UAP=m >> config.mk
endif

	cd $(DIR_APP) && make KLIB=/lib/modules/$(KVER)-$(VERSUFIX) \
		KMODPATH_ARG='INSTALL_MOD_PATH=' KMODDIR=kernel install-modules

	# Install firmware udev files...
	cd $(DIR_APP)/udev && install -m 644 50-compat_firmware.rules /etc/udev/rules.d/
	cd $(DIR_APP)/udev && install -m 755 compat_firmware.sh /lib/udev/

	-rm -rf $(DIR_APP)
	@$(POSTBUILD)
